var express = require('express');
var path = require('path');
var serveStatic = require('serve-static');
var port = process.env.PORT || 3000;
var nodemailer = require('nodemailer');

var app = express();
app.use(serveStatic(path.join(__dirname, 'dist')));

app.set('views', __dirname + '/dist');
app.engine('html', require('ejs').renderFile);

app.get('/contact', function(req, res) {
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport({
        pool: true,
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // use TLS
        auth: {
            user: 'bouallous.marwein@gmail.com',
            pass: '20042014N'
        }
    });

    // setup email data with unicode symbols
    var mailOptions = {
        from: '"App JS" <bouallous.marwein@gmail.com>', // sender address
        to: 'bouallous.marwein@gmail.com', // list of receivers
        subject: 'Hello ✔', // Subject line
        text: 'Hello world ?', // plain text body
        html: '<b>Hello world ?</b>' // html body
    };


    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });

});

app.get('/', function(req, res) {
    res.render('index.html');
});
app.listen(port);
console.log('server started http://localhost:' + port + '/');