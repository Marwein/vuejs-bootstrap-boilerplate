import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/Hello';

Vue.use(Router);

export default new Router({
    history: true,
    mode: 'hash',
    routes: [{
            path: '/',
            name: 'Accueil',
            redirect: '/hello'
        },
        {
            path: '/hello',
            name: 'Hello',
            component: Hello
        }
    ]
});