'use strict';

var express = require('express'),
    exphbs = require('express-handlebars'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    session = require('express-session'),
    passport = require('passport'),
    ajaxOnly = require('ajax-only'),
    LocalStrategy = require('passport-local'),
    TwitterStrategy = require('passport-twitter'),
    GoogleStrategy = require('passport-google'),
    path = require('path'),
    ejs = require('ejs'),
    FacebookStrategy = require('passport-facebook');

//We will be creating these two files shortly
// var config = require('./config.js'), //config file contains all tokens and other private info
//    funct = require('./functions.js'); //funct file contains our helper functions for our Passport and database work

var config = require('./config/database.js'), //config file contains all tokens and other private info
    funct = require('./config/functions.js'); //funct file contains our helper functions for our Passport and database work

var app = express();

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    req.session.error = 'Please sign in!';
    res.redirect('/signin');
}

//===============PASSPORT===============


// Use the LocalStrategy within Passport to login/"signin" users.
passport.use('local-signin', new LocalStrategy({ passReqToCallback: true }, //allows us to pass back the request to the callback
    function(req, username, password, done) {
        funct.localAuth(username, password)
            .then(function(user) {
                if (user) {
                    console.log("LOGGED IN AS: " + user.username);
                    req.session.success = 'You are successfully logged in ' + user.username + '!';
                    done(null, user);
                }
                if (!user) {
                    console.log("COULD NOT LOG IN");
                    req.session.error = 'Could not log user in. Please try again.'; //inform user could not log them in
                    done(null, user);
                }
            })
            .fail(function(err) {
                console.log(err.body);
            });
    }
));
// Use the LocalStrategy within Passport to register/"signup" users.
passport.use('local-signup', new LocalStrategy({ passReqToCallback: true }, //allows us to pass back the request to the callback
    function(req, username, password, done) {
        funct.localReg(username, password)
            .then(function(user) {
                if (user) {
                    console.log("REGISTERED: " + user.username);
                    req.session.success = 'You are successfully registered and logged in ' + user.username + '!';
                    done(null, user);
                }
                if (!user) {
                    console.log("COULD NOT REGISTER");
                    req.session.error = 'That username is already in use, please try a different one.'; //inform user could not log them in
                    done(null, user);
                }
            })
            .fail(function(err) {
                console.log(err.body);
            });
    }
));

passport.serializeUser(function(user, done) {
    console.log("serializing " + user.username);
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    console.log("deserializing " + obj);
    done(null, obj);
});

//===============EXPRESS================
// Configure Express
app.engine('html', ejs.renderFile);

app.use(logger('combined'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(session({ secret: 'RhWPzhulwE5EaA51fYSQaqKqiWZO6Qjv', saveUninitialized: true, resave: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, '../dist/')));
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.set({
    'views': path.join(__dirname, '../dist/'),
    'view engine': 'html'
});

// Session-persisted message middleware
app.use(function(req, res, next) {
    var err = req.session.error,
        msg = req.session.notice,
        success = req.session.success;

    delete req.session.error;
    delete req.session.success;
    delete req.session.notice;

    if (err) res.locals.error = err;
    if (msg) res.locals.notice = msg;
    if (success) res.locals.success = success;

    next();
});

//===============ROUTES===============

var api = express.Router();
var router = express.Router();
var options = {
    redirect: '/',
};
router
    .get('/', function(req, res) {
        res.render('index.html', { user: req.user });
    })
    .get('/login', function(req, res) {
        res.redirect('/#/login');
    })
    .get('/register', function(req, res) {
        res.redirect('/#/register');
    })
    .post('/login', function(req, res) {
        res.redirect('/#/login');
    })
    .post('/register', function(req, res) {
        res.redirect('/#/register');
    })
    .get('/logout', function(req, res) {
        res.redirect('/#/logout');
    });

api
    .get('/*', ajaxOnly(options))
    .get('/', function(req, res) {
        res.redirect('/');
    })
    .get('/user', function(req, res) {
        res.format({
            json: function() {
                res.json({ page: 'user page' });
            }
        });
    })
    .post('/login', passport.authenticate('local-signin', {
        successRedirect: '/',
        failureRedirect: '/signin'
    }));

app.use('/', router);
app.use('/api', api);


//===============PORT=================
var port = process.env.PORT || 5000; //select your port or let it pull from your .env file
app.listen(port);
console.log('server started http://localhost:' + port + '/');